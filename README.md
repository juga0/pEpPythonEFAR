*DEPRECATED*: moved to https://gitea.pep.foundation/juga/pEpPythonMixnet

Email Forwarder to Anonymous Remailers (`efar`)
===============================================

`efar` forwards Emails to [Cypherpunk anonymous remailer]s.

It is intented to run as Postfix pipe.

[Contributing](CONTRIBUTING.md)

License
-------

GPLv3

Authors
-------

juga at riseup dot net

[Cypherpunk anonymous remailer]: https://en.wikipedia.org/wiki/Cypherpunk_anonymous_remailer
