Maintainers
============

Maintainers are authors or contributors that can merge code or documentation
in the repository and release versions.
The current maintainers are:

juga at riseup dot net
