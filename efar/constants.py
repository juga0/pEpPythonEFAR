"""Constants."""
import os


# XXX: Create these first constants as part of the adapter
OUTGOING_MSG = 1
INCOMING_MSG = 2

ANGLE_ADDR = "<{}>"
NAME_ADDR = "{} {}"

PEP_NAME = "myself"
PEP_ADDRESS = "myself@pep.fundation"

BOB_NAME = "Bob Babagge"
BOB_ADDRESS = "bob@openpgp.example"
BOB_FP = "D1A66E1A23B182C9980F788CFBFCC82A015E7330"
BOB_NAME_ADDR = NAME_ADDR.format(BOB_NAME, ANGLE_ADDR.format(BOB_ADDRESS))

ALICE_NAME = "Alice Lovelace"
ALICE_ADDRESS = "alice@openpgp.example"
ALICE_FP = "EB85BB5FA33A75E15E944E63F231550C4F47E38E"
ALICE_NAME_ADDR = NAME_ADDR.format(ALICE_NAME,
                                   ANGLE_ADDR.format(ALICE_ADDRESS))

SUBJECT = "This is a subject"
BODY = "Hi world!\n"

REMAILER_KEY_SUBJECT = "remailer-key"
REMAILER_BODY = """::
Anon-To: {to}

##
Subject: {subject}

{body}"""
REMAILER_ENCRYPTED_PREFIX = """::
Encrypted: PGP

{}
"""

REMAILER_ADDRESS = "remailer@dizum.com"
REMAILER_NAME_ADDR = "Nomen Nescio <remailer@dizum.com>"
REMAILER_FP = "69AAA31095EA74254AF6BEBCDE7947C81698D34C"
NUMBER_REMAILERS = 5

FORWARDER_HOME = os.path.join(os.environ.get("HOME", '/home/user'), ".forwarder")
GNUPGHOME = os.path.join(FORWARDER_HOME, 'gnupg')
REMAILER_KEYS = os.path.join(os.path.dirname(__file__), '..', 'data',
                             'pgp-all.asc')

# XXX: sendmail, smtp, directly remailer
TRANSPORT = "sendmail"
SENDMAIL = "/usr/sbin/sendmail"

# Custom logging config
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'simple': {
            'format': '{levelname} {message}',
            'style': '{',
        },
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        },
    },
    'loggers': {
        'remailer': {
            'level': 'DEBUG',
            'handlers': ['console'],
            'propagate': True,
        },
    }
}

