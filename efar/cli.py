#! /usr/bin/env python3
"""efar command line interface."""
import argparse
import logging
import logging.config
import os
import sys

import pEp

from . import __version__, constants, lib

logging.config.dictConfig(constants.LOGGING)
logger = logging.getLogger(__name__)


def main():
    parser = argparse.ArgumentParser(
        description='Email Forwarder to Anoymous Remailers.')
    parser.add_argument('-d', '--debug',
                        help='Set logging level to debug',
                        action='store_true')
    parser.add_argument('--version', action='version',
                        help='version',
                        version='%(prog)s ' + __version__)
    parser.add_argument('recipient', nargs='?',
                        help='Recipient email address.',
                        default=constants.ALICE_NAME_ADDR)
    parser.add_argument('reply_to', nargs='?',
                        help='Email address to send the answer back.',
                        default=constants.BOB_NAME_ADDR)
    parser.add_argument('user', nargs='?',
                        help='The user running this command, to know their'
                             ' $HOME.')
    parser.add_argument('-f', '--email_path',
                        help='Email file path to forward to remailers.')
    parser.add_argument('-s', '--subject',
                        help='Message subject.',
                        default=constants.SUBJECT)
    parser.add_argument('-b', '--body',
                        help='Message body.',
                        default=constants.BODY)
    parser.add_argument('-g', '--gnupghome',
                        help='The directory to set GNUPGHOME to.',
                        default=constants.GNUPGHOME)

    args = parser.parse_args()
    if args.debug:
        logger.setLevel(logging.DEBUG)
    logger.debug('remailer call with args %s', args)
    if args.user:
        os.environ['HOME'] = os.path.join('home', args.user)
    logger.debug('$HOME=%s', os.environ['HOME'])
    if args.gnupghome:
        os.makedirs(args.gnupghome, mode=0o0700, exist_ok=True)
        os.environ['GNUPGHOME'] = args.gnupghome
    logger.debug('GNUPGPHOME=%s', os.environ['GNUPGHOME'])

    file = email = subject = body = None
    if not (args.email_path or args.body or args.subject):
        file = sys.stdin
    elif args.email_path:
        file = args.email_path
    if file:
        logger.debug("reading fd")
        with open(file) as fd:
            email = email.message_from_file(fd)
    if email:
        recipient = email.get('From', None)
        body = email.get_payload()
        subject = email.get('Subject', None)
    else:
        recipient = args.recipient
        body = args.body
        subject = args.subject

    try:
        pEp.import_key(constants.REMAILER_KEYS)
    except RuntimeError as e:
        logger.warning(e)
    lib.forward(recipient, subject, body, args.reply_to)


if __name__ == '__main__':
    main()
