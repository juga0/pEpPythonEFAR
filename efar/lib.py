"""efar functions."""
# TODO:
# - Sign keys so that encrypting with gnupg doesn't complain
# - Create identities for the remailers
# - Use several remailers
# - Change name to forwarder
from email.message import EmailMessage
import logging
import subprocess
import smtplib

import pEp

from . import constants

logger = logging.getLogger(__name__)


# XXX: Replace with adapter encrypt function when there is one to encrypt
# without generating MIME structure, or obtain the body from the pep Message.
def encrypt(remailer_fp, text):
    with open('/tmp/foo', 'w') as f:
        f.write(text)
    subprocess.call(["gpg", "--encrypt",
                     "--armor", "-r", remailer_fp, '/tmp/foo'])
    with open('/tmp/foo.asc') as f:
        cyphertext = f.read()
    return cyphertext


def select_remailer():
    pass


def select_remailers(number=constants.NUMBER_REMAILERS):
    pass


def forward(to, subject, body, to_fp='', reply_to=None):
    """Forward an email to a cypherpunk remailer Type I.

    It follows these steps:
    2. Compose a plaintext message with the remailer template.
    3. Encrypt that message.
    4. Create a the message with the remailer cyphertext prefix and the
       previous cyphertext.
    5. Optionally repeat 3. and 4.
    6. Place the previous cyphertext into an Email
    7. Send the Email to a remailer.
    """
    # Step 2: Compose Message.
    # NOTE: This is different than a pEp message or Email
    remailer_message = constants.REMAILER_BODY \
        .format(to=to, subject=subject, body=body)
    logger.debug('Unencrypted message for remailer: %s', remailer_message)

    # Step 3: Encrypt Message.
    # Assuming that the remailer key is in the keyring and that is only this:
    cyphertext = encrypt(constants.REMAILER_FP, remailer_message)

    # Step 4: Compose Encrypted Message
    remailer_cyphertext = constants.REMAILER_ENCRYPTED_PREFIX \
        .format(cyphertext)
    logger.debug('Encrypted message for remailer..', remailer_cyphertext)

    # TODO: Repeat 3. and 4.

    # Step 6: Create an Email.
    email = EmailMessage()
    email.set_content(remailer_cyphertext)
    email['To'] = constants.REMAILER_ADDRESS
    email['From'] = to
    logger.debug('Final Email for remailer.', email)

    # Step 7: Send Email.
    s = smtplib.SMTP('localhost')
    s.send_message(email)
    s.quit()
