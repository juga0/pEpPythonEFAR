#!/usr/bin/env python3
import os.path
# Always prefer setuptools over distutils
from setuptools import setup, find_packages


here = os.path.abspath(os.path.dirname(__file__))


def version():
    with open(os.path.join(here, "efar", "__init__.py")) as fp:
        for line in fp:
            if "__version__" in line.strip():
                version = line.split("=", 1)[1].strip().strip("'")
                return version


setup(
    name='efar',
    version=version(),
    description='pEp Python Email Forwarder to Anoymous Remailers',
    long_description=open("README.md").read(),
    long_description_content_type="text/markdown",
    author='juga',
    author_email='juga@riseup.net',
    license='GPLv3',
    url="https://gitea.pep.foundation/juga/pEpPythonEFAR.git",
    classifiers=[
        'Development Status :: 4 - Beta',
        "Environment :: Console",
        'Intended Audience :: Developers',
        'Intended Audience :: System Administrators',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3.7',
        'Topic :: System :: Networking',
    ],
    packages=find_packages(),
    include_package_data=True,
    keywords='email pEp OpenPGP remailer mixmaster',
    python_requires='>=3.7',
    entry_points={
        'console_scripts': [
            'efar = efar.cli:main',
        ]
    },
    install_requires=[
        'pEp>=2.0',
    ],
    extras_require={
        'test': ['flake8', 'tox', 'pytest', 'coverage'],
        'doc': ['sphinx', 'pylint'],
    },
)
